import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main/main.component';
import { HeaderComponent } from './header/header.component';
import { LandingComponent } from './landing/landing.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [MainComponent, HeaderComponent, LandingComponent],
  exports: [MainComponent, HeaderComponent, LandingComponent]
})
export class PageModule { }
