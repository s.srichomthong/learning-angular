import { Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { MainComponent } from './page/main/main.component';
import { HeaderComponent } from './page/header/header.component';
import { LandingComponent } from './page/landing/landing.component';


export const AppRoutes: Routes = [
    {
        path: "",
        component: HeaderComponent
    },
    {
        path: "main",
        component: MainComponent
    },
    {
        path: "**",
        component: LandingComponent
    }
];